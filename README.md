# README #

RunUtil is a command-line utility to inspect binaries.

### Usage ###
```
$ RunUtil.exe -h
Usage: RunUtil -f filename -m (checksum | (find -s pattern))
Command-line utility to inspect binaries.

Options:
  -f=VALUE                   input file
  -m=VALUE                   find or checksum
  -s=VALUE                   search pattern (only for find method)
  -h                         show this message and exit
```
### External dependence ###
[NDesk.Options library](http://www.ndesk.org/Options)

### Режимы работы ###
```
Run –f filename –m find –s hello
```
Выводит все смещения в байтах (через пробел) для файла «filename», где расположена строка «hello»

```
Run –f filename –m checksum
```
Выводит сумму всех 32-х битных слов в файле

```
Run –h
```
Выводит справку о командах и параметрах