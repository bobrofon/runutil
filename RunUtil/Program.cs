﻿using System;
using System.IO;

namespace RunUtil
{
	class MainClass
	{
		public static void Main (string[] args)
		{
			var options = new OptionParser(args);

			try
			{
				using (var reader = new BinaryReader(File.Open(options.FileName, FileMode.Open)))
				{
					switch(options.Module)
					{
					case Modules.CHECKSUM:
						Console.WriteLine(ChecksumModule.Checksum(reader));
						break;
					case Modules.FIND:
						var offsets = FindModule.FindAllOffsets(reader, options.Pattern);
						Console.WriteLine(String.Join(" ", offsets));
						break;
					}
				}
			}
			catch (IOException e)
			{
				Console.Error.WriteLine(String.Format("{0}: {1}", options.FileName, e.Message));
			}
		}
	}
}
