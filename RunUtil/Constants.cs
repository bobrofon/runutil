﻿namespace RunUtil
{
	enum ExitCodes
	{
		Success = 0,
		Failure = -1
	}

	public static class Modules
	{
		public const string FIND = "find";
		public const string CHECKSUM = "checksum";
	}

	public static class Default
	{
		public const int BUFFER_SIZE = 2 * 1024 * 1024;
	}
}

