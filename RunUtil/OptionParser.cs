﻿using System;
using NDesk.Options;
using System.Diagnostics;

namespace RunUtil
{
	public class OptionParser
	{
		string module;
		string file_name;
		string pattern;

		public string Module { get { return module; } }
		public string FileName { get { return file_name; } }
		public string Pattern { get { return pattern; } }
		
		public OptionParser(string[] args) {
			var opt_parser = new OptionSet() {
				{"f=", "input file", f => file_name = f },
				{"m=", String.Format("{0} or {1}", Modules.FIND, Modules.CHECKSUM), m => module = m },
				{"s=", String.Format("search pattern (only for {0} method)", Modules.FIND), s => pattern = s }
			};
			opt_parser.Add("h", "show this message and exit", h => ShowHelpAndExit(opt_parser));

			try {
				opt_parser.Parse(args);
				ValidateConstrains();
			}
			catch (OptionException e) {
				ShowErrorHelpAndExit(e);
			}
		}

		void ValidateConstrains() {
			if (file_name == null) {
				throw new OptionException("Missing required option '-f'", "f");
			}
			if (module == null) {
				throw new OptionException("Missing required option '-m'", "m");
			} else if (module != Modules.FIND && module != Modules.CHECKSUM) {
				throw new OptionException("Invalid option value " + module, "m");
			} else if (module == Modules.FIND && pattern == null) {
				throw new OptionException("Missing required option '-s'", "s");
			}
		}

		static string AppName() {
			return Process.GetCurrentProcess().ProcessName;
		}

		static void ShowErrorHelpAndExit(Exception e) {
			Console.Error.Write (AppName() + ": ");
			Console.Error.WriteLine (e.Message);
			Console.Error.WriteLine ("Try '" + AppName() + " -h' for more information.");
			Environment.Exit( -1 );
		}

		static void ShowHelpAndExit (OptionSet p)
		{
			Console.WriteLine (String.Format("Usage: {0} -f filename -m ({1} | ({2} -s pattern))",
				AppName(), Modules.CHECKSUM, Modules.FIND));
			Console.WriteLine ("Command-line utility to inspect binaries.");
			Console.WriteLine ();
			Console.WriteLine ("Options:");
			p.WriteOptionDescriptions (Console.Out);
			Environment.Exit( 0 );
		}
	}
}

