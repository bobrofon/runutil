﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace RunUtil
{
	public static class FindModule
	{
		/*
		 * FindAllOffsets returns all offsets in the 'reader' stream where binary representation of patterns appears.
		 * Buffer is limited by Default.BUFFER SIZE.
		 */
		public static List<long> FindAllOffsets(BinaryReader reader, string pattern) {
			byte[] binary_pattern = Encoding.Default.GetBytes(pattern);
			byte[] buf = new byte[Math.Max(Default.BUFFER_SIZE, binary_pattern.Length * 2)];
			int reserved_bytes = 0;
			long total_read = 0;
			var offsets = new List<long>();

			for (;;) {
				var buf_len = FillBuf(reader, buf, reserved_bytes);
				if (buf_len < binary_pattern.Length) {
					break;
				}
				for (int i = 0; i < buf_len - binary_pattern.Length; ++i) {
					if (Match(buf, binary_pattern, i)) {
						offsets.Add(total_read + i);
					}
				}

				reserved_bytes = binary_pattern.Length - 1;
				total_read += buf_len - reserved_bytes;
				Buffer.BlockCopy(buf, buf.Length - reserved_bytes, buf, 0, reserved_bytes);
			}

			return offsets;
		}

		/*
		 * FillBuf reads bytes from the reader in the byte array starts with offset.
		 * FillBuf returns the total number of bytes into the buffer. 
		 * Return value might be less than the length of array only if the end of the stream is reached.
		 */
		static int FillBuf(BinaryReader reader, byte[] buf, int offset) {
			int total_len = offset;
			int current_len = -1;

			while (current_len != 0 && buf.Length > total_len) {
				current_len = reader.Read(buf, total_len, buf.Length - total_len);
				total_len += current_len;
			}

			return total_len;
		}

		/*
		 * Match is an implementation of brute force string matching algorithm.
		 */
		static bool Match(byte[] buf, byte[] pattern, int offset) {
			for (int i = 0; i < pattern.Length; ++i) {
				if (buf[offset + i] != pattern[i]) {
					return false;
				}
			}
			return true;
		}
	}
}

