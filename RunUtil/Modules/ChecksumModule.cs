﻿using System;
using System.IO;

namespace RunUtil
{
	public static class ChecksumModule
	{
		/*
		 * Checksum computes 32-bit hash for reader.
		 * reader represented as a stream of integers in little-endian format.
		 */
		public static Int32 Checksum(BinaryReader reader) {
			Int32 checksum = 0;
			byte[] buf = new byte[Default.BUFFER_SIZE];
			int buf_len = -1;
			Int32 next_int = 0;
			int next_int_len = 0;

			while(buf_len != 0) {
				buf_len = reader.Read(buf, 0, buf.Length);
				for (int i = 0; i < buf_len; ++i) {
					next_int += (buf[i] << next_int_len);
					next_int_len += 8;
					if (next_int_len == 4) {
						checksum += next_int;
						next_int = 0;
						next_int_len = 0;
					}
				}
			}
			checksum += next_int;

			return checksum;
		}
	}
}

